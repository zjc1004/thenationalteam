/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author wangyixuan
 */
public class Flight {
    private String destination;
    private String airliner;
    private String departure;
    private String model;
    private Date departuretime;
    private Date arrivaltime;
    private String flightnum;
    private int totalseat;
    private int price;

    public Flight(){
        
    }
    
    public int getReseatnum() {
        return reseatnum;
    }

    public void setReseatnum(int reseatnum) {
        this.reseatnum = reseatnum;
    }
    private int reseatnum;
    private List<String> bookname = new ArrayList<>();
    private String[][] seat = new String[25][6]; // first - row#, second - column#
    private int[][] seatz = new int[25][6];

    public Flight(String destination, String airline, String departure, String model, Date departuretime,
            Date arrivaltime, String flightnum, int reseatnum, int totalseat, int price) {
        this.destination = destination;
        this.airliner = airline;
        this.departuretime = departuretime;
        this.departure = departure;
        this.model = model;
        this.arrivaltime = arrivaltime;
        this.flightnum = flightnum;
        this.reseatnum = reseatnum;
        this.totalseat=totalseat;
        this.price=price;
//        for (int i = 0; i < 25; i++) {
//            for (int j = 0; j < 6; j++) {
//                this.seat[i][j] = "";
//            }
//        }
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < 6; j++) {
                this.seatz[i][j] = 1;
            }
        }
    }

    public int getTotalseat() {
        return totalseat;
    }

    public void setTotalseat(int totalseat) {
        this.totalseat = totalseat;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    public int countseat(){
        int count=0;
        for (int i = 0; i < seatz.length; i++) {
            for (int j = 0; j < seatz[0].length; j++) {
                if (seatz[i][j]==1) {
                 count++;   
                }
            }
        }
        return count;
    }
    
    public boolean findRemain(int row, int column){
        if (seatz[row][column]==0) {
            return false;
        }
        return true;
    }
    
    public void buyseat(int a,int b){
        seatz[a][b]=0;
    }
    

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airline) {
        this.airliner = airline;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(Date departuretime) {
        this.departuretime = departuretime;
    }

    public Date getArrivaltime() {
        return arrivaltime;
    }

    public void setArrivaltime(Date arrivaltime) {
        this.arrivaltime = arrivaltime;
    }

    public String getFlightnum() {
        return flightnum;
    }

    public void setFlightnum(String flightnum) {
        this.flightnum = flightnum;
    }

    public boolean customerBook(String name, int row, int column) {
        this.bookname.add(name);
        this.seat[row][column] = name;
        return true;
    }

    public String agencyQueryBook() {
        return this.bookname.toString();
    }

    public List<int[]> getRemainingSeat() {
        // return all the seats in int[0](row), int[1](column).
        List<int[]> rem = new ArrayList<int[]>();
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < 6; j++) {
                if (this.seat[i][j] == "") {
                    int[] _temp = new int[]{i,j};
                    rem.add(_temp); 
                }
            }
        }
        return rem;
    }

    @Override
    public String toString() {
        return this.getAirliner();
    }
}

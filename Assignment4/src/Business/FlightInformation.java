/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Comparator.comparing;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 *
 * @author wangyixuan
 */
public class FlightInformation {
    
    public List<Flight> flightList;
    private FlightInformation abc;
    
    public FlightInformation() throws ParseException{
        
        flightList = new ArrayList<>();
        
          DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime1 = dateFormat1.parse("06-12-2019 14:24");
          DateFormat dateFormat2 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime1 = dateFormat2.parse("06-12-2019 20:00");
          
          DateFormat dateFormat3 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime2 = dateFormat3.parse("06-12-2019 11:00");
          DateFormat dateFormat4 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime2 = dateFormat4.parse("06-12-2019 12:00");
          
          DateFormat dateFormat5 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime3 = dateFormat5.parse("06-17-2019 21:00");
          DateFormat dateFormat6 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime3 = dateFormat6.parse("06-17-2019 12:00");
        
        Flight flight1 = new Flight("Boston", "UA", "LA", "Boeing757", departuretime1, arrivaltime1, "UA824",150,150);
        Flight flight2 = new Flight("Boston", "UA", "NY", "Boeing757", departuretime2, arrivaltime2, "UA827",150,150);
        Flight flight3 = new Flight("Miami", "DAL", "NY", "Boeing777", departuretime3, arrivaltime3, "DL181",150,150);
        Flight flight4 = new Flight("NY", "DAL", "LA", "Airbus321", date7, date8, "DL188",150);
        Flight flight5 = new Flight("LAS", "AA", "LA", "Airbus321", date9, date10, "AA172",150);
        Flight flight6 = new Flight("NY", "AA", "Miami", "Boeing777", date11, date12, "AA573",150);
        
        flightList.add(flight1);
        flightList.add(flight2);
        flightList.add(flight3);
        flightList.add(flight4);
        flightList.add(flight5);
        flightList.add(flight6);
    }     
    
    public List<Flight> getList(){
        return flightList;
    }
    
    public Flight addFlight(){
        Flight newFlight = new Flight();
        flightList.add(newFlight);
        return newFlight;
    }
    public FlightInformation getflightList() throws ParseException{
      abc = new FlightInformation();
      return abc;
    }
    
    public List<Flight> getSingle(){
        List<Flight> tempList = flightList.stream().collect(
            collectingAndThen(toCollection(() -> new TreeSet<>(comparing(Flight::getAirliner))), ArrayList::new));
        return tempList;
    }
    
     public List<Flight> FindSelected(String selected){
        ArrayList<Flight> tempList = new ArrayList<>();
        for(Flight a:flightList){
            if(a.getAirliner().equals(selected)){
             tempList.add(a);
            }
        }
        return tempList;
    }
     
    public List<Flight> getSearchList(String from, String to){
        ArrayList<Flight> tempList = new ArrayList<>();
        for(Flight vs:flightList){
            if(vs.getDeparture().equals(from) && vs.getDestination().equals(to)){
             tempList.add(vs);
            }
        }
        return tempList;
    }
    
       public void deleteFlight(Flight a){
        flightList.remove(a);
    }
     
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author JackZhang
 */
public class CustomerDirectory {
    private ArrayList<Customer> customerList;
    
    public CustomerDirectory() {
        this.customerList = new ArrayList<Customer>();
        Customer cu1=new Customer("test1", "test1",0,0, "");
        Customer cu2=new Customer("test2", "test2",0,0, "");
        Customer cu3=new Customer("test3", "test3",0,0, "");
        
        customerList.add(cu1);
        customerList.add(cu2);
        customerList.add(cu3);
    }
    
    public ArrayList<Customer> getCustomerDirectory() {
        return this.customerList;
        
    }
    
//    public void addCustomer(String username, String password) {
//        Customer user=new Customer(username, password);
//        this.customerList.add(user);
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author wangyixuan
 */
public class Customer {
   
    private String username;
    private String password;
    private String name;
    private int row;
    private int colunm;
    private String airliner;
    //private ArrayList<Seat> seats;

    public Customer() {

    }
    
    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColunm() {
        return colunm;
    }

    public void setColunm(int colunm) {
        this.colunm = colunm;
    }
    
    public Customer(String username, String password, int row, int colunm, String airliner) {
        this.username = username;
        this.password = password;
        this.row = row;
        this.colunm = colunm;
        this.airliner = airliner;
        //this.name = name;
        //this.seats = new ArrayList<Seat>();
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }
    
//    public ArrayList<Seat> getSeats() {
//        return this.seats;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

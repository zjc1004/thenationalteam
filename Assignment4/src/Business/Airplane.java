/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author JackZhang
 */
public class Airplane {
    
    private String airliner;
    private String model;
    private int row;
    private int column;
    private int totalSeatNum;
    private int remainedSeatNum;
    
    public Airplane(String airliner, String model, int row,int column, int totalSeatNum, int remainedSeatNum) {
        this.airliner = airliner;
        this.model = model;
        this.row = row;
        this.column = column;
        this.model = model;
        this.totalSeatNum = totalSeatNum;
        this.remainedSeatNum = remainedSeatNum;
    }

    public Airplane(){
        
    }
    
    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getTotalSeatNum() {
        return totalSeatNum;
    }

    public void setTotalSeatNum(int totalSeatNum) {
        this.totalSeatNum = totalSeatNum;
    }

    public int getRemainedSeatNum() {
        return remainedSeatNum;
    }

    public void setRemainedSeatNum(int remainedSeatNum) {
        this.remainedSeatNum = remainedSeatNum;
    }
    
    
    @Override
    public String toString(){
        return this.getAirliner();
    }
    
    
    
}

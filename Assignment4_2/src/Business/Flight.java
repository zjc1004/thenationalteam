/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author wangyixuan
 */
public class Flight {
    private String destination;
    private String airliner;
    private String departure;
    private String model;
    private Date departuretime;
    private Date arrivaltime;
    private String flightnum;
    
    public Flight(){
        
    }
    
    public Flight(String destination, String airliner, String departure, String model, Date departuretime, Date arrivaltime, String flightnum) {
        this.destination = destination;
        this.airliner = airliner;
        this.departuretime = departuretime;
        this.departure = departure;
        this.model = model;
        this.arrivaltime = arrivaltime;
        this.flightnum = flightnum;
    }


    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airline) {
        this.airliner = airline;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(Date departuretime) {
        this.departuretime = departuretime;
    }

    public Date getArrivaltime() {
        return arrivaltime;
    }

    public void setArrivaltime(Date arrivaltime) {
        this.arrivaltime = arrivaltime;
    }

    public String getFlightnum() {
        return flightnum;
    }

    public void setFlightnum(String flightnum) {
        this.flightnum = flightnum;
    }
    
    
    @Override
    public String toString(){
        return this.getAirliner();
    }

   
}

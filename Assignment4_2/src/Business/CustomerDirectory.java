/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author JackZhang
 */
public class CustomerDirectory {
    private ArrayList<Customer> customerDirectory;
    
    public CustomerDirectory() {
        this.customerDirectory = new ArrayList<Customer>();
    }
    
    public ArrayList<Customer> getCustomerDirectory() {
        return this.customerDirectory;
    }
    
    public void addCustomer(String id, String username, String password, String name) {
        Customer user=new Customer(username, password, name);
        this.customerDirectory.add(user);
    }
}

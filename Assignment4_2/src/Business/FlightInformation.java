/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Comparator.comparing;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;
import Business.Flight;
/**
 *
 * @author wangyixuan
 */
public class FlightInformation {
    
    public List<Flight> flightList;
    private FlightInformation abc;
    
    public FlightInformation() throws ParseException{
        
        flightList = new ArrayList<>();
        
          DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime1 = dateFormat1.parse("06-12-2019 14:24");
          DateFormat dateFormat2 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime1 = dateFormat2.parse("06-12-2019 20:00");
          
          DateFormat dateFormat3 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime2 = dateFormat3.parse("06-12-2019 11:00");
          DateFormat dateFormat4 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime2 = dateFormat4.parse("06-12-2019 12:00");
          
          DateFormat dateFormat5 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime3 = dateFormat5.parse("06-17-2019 21:00");
          DateFormat dateFormat6 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime3 = dateFormat6.parse("06-17-2019 12:00");
//        String str7= "06/19/2019 10:00"; Date date7 = sdf.parse(str7);
//        String str8= "06/19/2019 14:01"; Date date8 = sdf.parse(str8);
//        String str9= "06/15/2019 13:00"; Date date9 = sdf.parse(str9);
//        String str10= "06/15/2019 14:30"; Date date10 = sdf.parse(str10);
//        String str11= "06/13/2019 17:00"; Date date11 = sdf.parse(str11);
//        String str12= "06/13/2019 20:30"; Date date12 = sdf.parse(str12);
        
        Flight flight1 = new Flight("Boston", "UA", "LA", "Boeing757", departuretime1, arrivaltime1, "UA824");
        Flight flight2 = new Flight("Boston", "UA", "NY", "Boeing757", departuretime2, arrivaltime2, "UA827");
        Flight flight3 = new Flight("Miami", "DAL", "NY", "Boeing777", departuretime3, arrivaltime3, "DL181");
//        Flight flight4 = new Flight("NY", "DAL", "LA", "Airbus321", date7, date8, "DL188");
//        Flight flight5 = new Flight("LAS", "AA", "LA", "Airbus321", date9, date10, "AA172");
//        Flight flight6 = new Flight("NY", "AA", "Miami", "Boeing777", date11, date12, "AA573");
        
        flightList.add(flight1);
        flightList.add(flight2);
        flightList.add(flight3);
//        flightList.add(flight4);
//        flightList.add(flight5);
//        flightList.add(flight6);
    }     
    
    public List<Flight> getList(){
        return flightList;
    }
    
    public Flight addFlight(){
         Flight newFlight = new Flight();
         flightList.add(newFlight);
         return newFlight;
    }
    public FlightInformation getflightList() throws ParseException{
      abc = new FlightInformation();
      return abc;
    }
    
    public List<Flight> getSingle(){
        List<Flight> tempList = flightList.stream().collect(
            collectingAndThen(toCollection(() -> new TreeSet<>(comparing(Flight::getAirliner))), ArrayList::new));
        return tempList;
    }
    
     public List<Flight> FindSelected(String selected){
        ArrayList<Flight> tempList = new ArrayList<>();
        for(Flight a:flightList){
            if(a.getAirliner().equals(selected)){
             tempList.add(a);
            }
        }
        return tempList;
    }
     
    public List<Flight> getSearchList(String from, String to){
        ArrayList<Flight> tempList = new ArrayList<>();
        for(Flight vs:flightList){
            if(vs.getDeparture().equals(from) && vs.getDestination().equals(to)){
             tempList.add(vs);
            }
        }
        return tempList;
    }
    
    public void deleteFlight(Flight a){
        flightList.remove(a);
    }
    
}

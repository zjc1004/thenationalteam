/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author wangyixuan
 */
public class Customer {
   
    private String username;
    private String password;
    private String name;
    //private ArrayList<Seat> seats;
    
    public Customer(String username, String password, String name) {
        this.username = username;
        this.password = password;
        this.name = name;
        //this.seats = new ArrayList<Seat>();
    }
    
    
    
//    public String getId() {
//        return this.id;
//    }

//    public void setId(String id) {
//        this.id = id;
//    }
    
    
//    public ArrayList<Seat> getSeats() {
//        return this.seats;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.item.Pet;
import business.item.ShopOrder;
import business.role.User;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author JackZhang
 */
public class PetFoodStore extends Organization
{
    private ConcurrentHashMap<String, Integer> petsInventory;
    
    public PetFoodStore(String name, String location){
        super(name, location);
        this.petsInventory = new ConcurrentHashMap<>();
    }
    
    public int checkRemaining(String pet){
        if (petsInventory.containsKey(pet))
            return petsInventory.get(pet);
        else
            return 0;
    }

    
    public void addPets(String pet, int amount) {
        int n = petsInventory.get(pet);
        petsInventory.put(pet, n + amount);
    }
    
    public int deletePets(String pet, int amount){
        int n = petsInventory.get(pet);
        if (n - amount <= 0){
            petsInventory.remove(pet);
            return n;
        }
        else{
            petsInventory.put(pet, n - amount);
            return amount;
        }
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    
}

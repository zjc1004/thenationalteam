/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.item.ForsterOrder;;
import business.role.User;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author JackZhang
 */

public class PetForster extends Organization{
    private ConcurrentHashMap<String, Integer> available;
    
    
    public PetForster(String name, String location){
        super(name, location);
        available = new ConcurrentHashMap<>();
    }
    
    public ForsterOrder addOrder(ForsterOrder order){
        System.out.println("(before) available =  " + available);
        String pet = order.getPet();
        System.out.println(" pet = " + pet);
        if (!available.containsKey(pet))
            return null;
        
        if (order.getAmount() > available.get(pet))
            order.setAmount(available.get(pet));
        available.put(pet, available.get(pet) - order.getAmount());
        System.out.println("(after) available =  " + available);
        return order;
    }
    
    public void loadForster(int cat, int dog, int rabbit){
        getAvailable().put("Cat", cat);
        getAvailable().put("Dog", dog);
        getAvailable().put("Rabbit", rabbit);
    }
    
    @Override
    public String toString() {
        return this.getName();
    }

    /**
     * @return the available
     */
    public ConcurrentHashMap<String, Integer> getAvailable() {
        return available;
    }

    /**
     * @param available the available to set
     */
    public void setAvailable(ConcurrentHashMap<String, Integer> available) {
        this.available = available;
    }
    
    
    
}

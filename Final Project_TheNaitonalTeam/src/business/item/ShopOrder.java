/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.item;
import business.organization.PetFoodStore;
import business.role.User;
import java.util.concurrent.ConcurrentHashMap;


/**
 *
 * @author JackZhang
 */
public class ShopOrder extends Item{

    /**
     * @return the cart
     */
    public ConcurrentHashMap<String, Integer> getCart() {
        return cart;
    }

    /**
     * @param cart the cart to set
     */
    public void setCart(ConcurrentHashMap<String, Integer> cart) {
        this.cart = cart;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName the storeName to set
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }
    private ConcurrentHashMap<String, Integer> cart;
    private String addr, phone, user, storeName, location;
    /**
     * @return the addr
     */
    public ShopOrder(){
        super();
        cart = new ConcurrentHashMap<>();
    }

    public String getAddr() {
        return addr;
    }

    /**
     * @param addr the addr to set
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    
    
    
}

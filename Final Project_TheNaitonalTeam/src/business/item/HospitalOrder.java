/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.item;

import business.organization.Hospital;
import business.role.User;

/**
 *
 * @author JackZhang
 */
public class HospitalOrder extends Item{
    private String user, hospitalName, loaction, category, descryption, date, month;
    
    public HospitalOrder(){
        
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the descryption
     */
    public String getDescryption() {
        return descryption;
    }

    /**
     * @param descryption the descryption to set
     */
    public void setDescryption(String descryption) {
        this.descryption = descryption;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the hospitalName
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * @param hospitalName the hospitalName to set
     */
    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    /**
     * @return the loaction
     */
    public String getLoaction() {
        return loaction;
    }

    /**
     * @param loaction the loaction to set
     */
    public void setLoaction(String loaction) {
        this.loaction = loaction;
    }

    /**
     * @return the user
     */
    
    
    
}

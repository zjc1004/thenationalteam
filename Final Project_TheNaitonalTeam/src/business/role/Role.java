/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.role;

/**
 *
 * @author JackZhang
 */
public class Role {
    // private UUID id;
    private String username, password;
    
    public Role(String un, String pw){
        //this.id = UUID.randomUUID();
        this.username = un;
        this.password = pw;
    }

    /**
     * @return the id
     */
//    public UUID getId() {
//        return id;
//    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    
}

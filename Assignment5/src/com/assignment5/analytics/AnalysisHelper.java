/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author JackZhang
 */
public class AnalysisHelper {
    
    private int qua[] = new int[5000];
    
    public void Question1() {
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Product> productmap = DataStore.getInstance().getProducts();
        Map<Integer, Integer> productNum = new HashMap<>();

        Iterator<Item> it = items.values().iterator();
        while (it.hasNext()) {
            Item next = it.next();
            int productId = next.getProductId();
            int price = next.getSalesPrice();
            if (price > productmap.get(productId).getTargetPrice()) {
                if (productNum.containsKey(productId)) {
                    int newprice = productNum.get(productId) + next.getQuantity();
                    productNum.remove(productId);
                    productNum.put(productId, newprice);
                } else {
                    productNum.put(productId, next.getQuantity());
                }
            }
        }
        
        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(productNum.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o2.getValue() - o1.getValue();
        }
    });
        //System.out.println(list);
        System.out.println();
        System.out.println("Question 1_Our top 3 best negotiated products: ");
        int count=0;
        for (int i = 0; i < list.size(); i++) {
            System.out.print(count+1+".");   
            System.out.println("Product " + list.get(i).getKey() + ", sale quantity " + list.get(i).getValue());
            int j=i+1;
            while(list.get(j).getValue().equals(list.get(i).getValue())) {
                System.out.println("Product " + list.get(i).getKey() + ", sale quantity " + list.get(i).getValue());
                j++;
            }
            i=j-1; count++;
            System.out.println();
            if (count==3) break;
        }
    }
    
    public void Question2(){
        Map<Integer, Order> orderMap = DataStore.getInstance().getOrders();
        Map<Integer, Integer> bestCustomer = new HashMap<>();
        Map<Integer, Product> productmap = DataStore.getInstance().getProducts();

        Iterator<Map.Entry<Integer, Order>> it = orderMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, Order> next = it.next();
            int customerId = next.getValue().getCustomerId();
            int price = next.getValue().getIt().getSalesPrice();
            int productId = next.getValue().getIt().getProductId();
            int AbsPrice = Math.abs(price - productmap.get(productId).getTargetPrice());
            if (bestCustomer.containsKey(customerId)) {
                    int newprice = bestCustomer.get(customerId) + AbsPrice;
                    bestCustomer.remove(customerId);
                    bestCustomer.put(customerId, newprice);
                } else {
                    bestCustomer.put(customerId, AbsPrice);
                }
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(bestCustomer.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o1.getValue() - o2.getValue();
        }
    });
        
        System.out.println();
        //System.out.println(list);
        System.out.println("Question 2_Our 3 best customers: ");
        int count=0;
        for (int i = 0; i < list.size(); i++) {
            System.out.print(count+1+".");   
            System.out.println("Customer " + list.get(i).getKey() + ", absolute value " + list.get(i).getValue());
            int j=i+1;
            while(list.get(j).getValue().equals(list.get(i).getValue())) {
                System.out.println("Customer " + list.get(i).getKey() + ", absolute value " + list.get(i).getValue());
                j++;
            }
            i=j-1; count++;
            System.out.println();
            if (count==3) break;
        }  
    }
    
    public void Question3(){
        Map<Integer, Order> ordermap = DataStore.getInstance().getOrders();
        Map<Integer, Integer> bestSales = new HashMap<>();
        Map<Integer, Product> productmap = DataStore.getInstance().getProducts();

        Iterator<Map.Entry<Integer, Order>> it = ordermap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, Order> next = it.next();
            int salesId = next.getValue().getSalesId();
            int price = next.getValue().getIt().getSalesPrice();
            int productId = next.getValue().getIt().getProductId();
            int quantity = next.getValue().getIt().getQuantity();
            int profit = (price - productmap.get(productId).getTargetPrice()) * quantity;
            if (bestSales.containsKey(salesId)) {
                    int newprice = bestSales.get(salesId) + profit;
                    bestSales.remove(salesId);
                    bestSales.put(salesId, newprice);
                } else {
                    bestSales.put(salesId, profit);
                }
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(bestSales.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o2.getValue() - o1.getValue();
        }
    });
        
        System.out.println();
        //System.out.println(list);
        System.out.println("Question 3_Our 3 best sales people: ");
        int count=0;
        for (int i = 0; i < list.size(); i++) {
            System.out.print(count+1+".");   
            System.out.println("Salesman " + list.get(i).getKey() + ", sales profit " + list.get(i).getValue());
            int j=i+1;
            while(list.get(j).getValue().equals(list.get(i).getValue())) {
                System.out.println("Salesman " + list.get(i).getKey() + ", sales profit " + list.get(i).getValue());
                j++;
            }
            i=j-1; count++;
            System.out.println();
            if (count==3) break;
        }  
    }
    
    public void Question4(){
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Product> productmap = DataStore.getInstance().getProducts();
        int totalRevenue = 0;

        Iterator<Order> it = orders.values().iterator();
        while (it.hasNext()) {
            Order order = it.next();
            int productId = order.getIt().getProductId();
            int price = order.getIt().getSalesPrice();
            int quantity = order.getIt().getQuantity();
            totalRevenue = totalRevenue + (price - productmap.get(productId).getTargetPrice())*quantity;
        }
        System.out.println();
        System.out.println("Question 4_Our total revenue for the year: " + totalRevenue);
        System.out.println();
    }
    
    public void Question5(){
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Product> productMap = DataStore.getInstance().getProducts();
        Map<Integer, Integer> res = new HashMap<>();
        
        for(int i=0; i<qua.length; i++) qua[i]=0;
        
        Iterator<Item> it = items.values().iterator();
        while (it.hasNext()) {
            Item next = it.next();
            int productId = next.getProductId();
            int price = next.getSalesPrice();
            int quantity = next.getQuantity();
            int sum = price * quantity;
            res.put(productId, res.getOrDefault(productId, sum) + sum);
            qua[productId] += quantity;
        }
        
        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(res.entrySet());

//  Original Table
        System.out.println("\n");
        System.out.println("Original Table:");
        System.out.println("ProdunctId\t" + "SalesPrice\t" + "TargetPrice\t" + "Difference\t");
        
        HashMap<Integer, Integer> dif=new HashMap<>();
        HashMap<Integer, Integer> sale=new HashMap<>();
        //HashMap<Integer, Double> err=new HashMap<>();
        
        for(int i=0; i< list.size(); i++){
            int productId = list.get(i).getKey();
            int avasale = list.get(i).getValue()/qua[productId];
            int targetPrice = productMap.get(productId).getTargetPrice();
            int diff = targetPrice - avasale;
//            DecimalFormat df = new DecimalFormat("#.000");
//            double error = Double.parseDouble(df.format(Math.abs(diff)/avasale));
            if(avasale < targetPrice){
                dif.put(productId, diff);
                sale.put(productId, avasale);
//                err.put(productId, error);
            }
        }
        
        List<Map.Entry<Integer, Integer>> temp = new ArrayList<>(dif.entrySet());
        Collections.sort(temp, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });
        
        for(int i=0; i< temp.size(); i++){
            int productId = temp.get(i).getKey();
            int avasale = sale.get(productId);
            int targetPrice = productMap.get(productId).getTargetPrice();
            int difference = temp.get(i).getValue();
//            double error = err.get(productId);
            System.out.println("\t" + productId + "\t" + avasale + "\t\t" + targetPrice + "\t\t" + difference + "\t\t"); 
        }
        
        
        System.out.println("----------------------------------------------------------------");
        
        dif.clear();
        sale.clear();
        
        for(int i=0; i< list.size(); i++){
            int productId = list.get(i).getKey();
            int avasale = list.get(i).getValue()/qua[productId];
            int targetPrice = productMap.get(productId).getTargetPrice();
            int diff = targetPrice - avasale;
//            DecimalFormat df = new DecimalFormat("#.000");
//            double error = Double.parseDouble(df.format(Math.abs(diff)/avasale));
            if(avasale >= targetPrice){
                dif.put(productId, diff);
                sale.put(productId, avasale);
//                err.put(productId, error);
            }
        }
        
        List<Map.Entry<Integer, Integer>> temp2 = new ArrayList<>(dif.entrySet());
        Collections.sort(temp2, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o1.getValue() - o2.getValue();
            }
        });
        
        for(int i=0; i< temp2.size(); i++){
            int productId = temp2.get(i).getKey();
            int avasale = sale.get(productId);
            int targetPrice = productMap.get(productId).getTargetPrice();
            int difference = temp2.get(i).getValue();
//            double error = err.get(productId);
            System.out.println("\t" + productId + "\t" + avasale + "\t\t" + targetPrice + "\t\t" + difference + "\t\t"); 
        }
        


//  Modify Table
        System.out.println("\n");
        System.out.println("Modify Table:");
        System.out.println("ProdunctId\t" + "SalesPrice\t" + "TargetPrice\t" + "Difference\t" + "error");
        for(int i=0; i< list.size(); i++){
            int productId = list.get(i).getKey();
            double avasale = list.get(i).getValue()/qua[productId];
            //double targetPrice = productMap.get(productId).getTargetPrice();
            double targetPrice = avasale;   //Set the target price to salesprice
            double diff = targetPrice - avasale;
            DecimalFormat df = new DecimalFormat("#.00");
            double error = Double.parseDouble(df.format(Math.abs(diff)/avasale));
            System.out.println("\t" + productId + "\t" + avasale + "\t\t" + targetPrice + "\t\t" + diff + "\t\t" + error+"%");
        }
    }
}

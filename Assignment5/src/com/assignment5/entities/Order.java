/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Order {
    
    int orderId;
    int salesId;
    int customerId;
    Item it;


    public Order(int orderId, int salesId, int customerId, Item item) {
        this.orderId = orderId;
        this.salesId = salesId;
        this.customerId = customerId;
        it = item;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }
    

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    
    public Item getIt() {
        return it;
    }

    public void setIt(Item it) {
        this.it = it;
    }
}

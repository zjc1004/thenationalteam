/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Comparator.comparing;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 *
 * @author wangyixuan
 */
public class FlightInformation {
    
    public List<Flight> flightList;
    private FlightInformation abc;
    
    public FlightInformation() throws ParseException{
        
        flightList = new ArrayList<>();
        
          DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime1 = dateFormat1.parse("06-12-2019 14:24");
          DateFormat dateFormat2 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime1 = dateFormat2.parse("06-12-2019 20:00");
          
          DateFormat dateFormat3 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime2 = dateFormat3.parse("06-12-2019 11:00");
          DateFormat dateFormat4 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime2 = dateFormat4.parse("06-12-2019 12:00");
          
          DateFormat dateFormat5 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime3 = dateFormat5.parse("06-17-2019 21:00");
          DateFormat dateFormat6 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime3 = dateFormat6.parse("06-17-2019 12:00");
          
          DateFormat dateFormat7 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime4 = dateFormat7.parse("06-19-2019 10:00");
          DateFormat dateFormat8 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime4 = dateFormat8.parse("06-19-2019 14:00");
          
          DateFormat dateFormat9 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime5 = dateFormat9.parse("06-15-2019 13:00");
          DateFormat dateFormat10 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime5 = dateFormat10.parse("06-15-2019 14:30");
          
          DateFormat dateFormat11 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date departuretime6 = dateFormat11.parse("06-13-2019 17:30");
          DateFormat dateFormat12 = new SimpleDateFormat("MM-dd-yy HH:mm");
          Date arrivaltime6 = dateFormat12.parse("06-13-2019 20:00");
        
        Flight flight1 = new Flight("Boston", "UA", "LA", "Boeing757", departuretime1, arrivaltime1, "UA824",150,150,100);
        Flight flight2 = new Flight("Boston", "UA", "NY", "Boeing757", departuretime2, arrivaltime2, "UA827",150,150,100);
        Flight flight3 = new Flight("Miami", "DAL", "NY", "Boeing777", departuretime3, arrivaltime3, "DL181",150,150,100);
        Flight flight4 = new Flight("NY", "DAL", "LA", "Airbus321", departuretime4, arrivaltime4, "DL188",150,150,100);
        Flight flight5 = new Flight("LAS", "AA", "LA", "Airbus321", departuretime5, arrivaltime5, "AA172",150,150,100);
        Flight flight6 = new Flight("NY", "AA", "Miami", "Boeing777", departuretime6, arrivaltime6, "AA573",150,150,100);
        
        flightList.add(flight1);
        flightList.add(flight2);
        flightList.add(flight3);
        flightList.add(flight4);
        flightList.add(flight5);
        flightList.add(flight6);
    }     
    
    public List<Flight> getList(){
        return flightList;
    }
    
    public Flight addFlight(){
        Flight newFlight = new Flight();
        flightList.add(newFlight);
        return newFlight;
    }
    public FlightInformation getflightList() throws ParseException{
      abc = new FlightInformation();
      return abc;
    }
    
    public List<Flight> getSingle(){
        List<Flight> tempList = flightList.stream().collect(
            collectingAndThen(toCollection(() -> new TreeSet<>(comparing(Flight::getAirliner))), ArrayList::new));
        return tempList;
    }
    
     public List<Flight> FindSelected(String selected){
        ArrayList<Flight> tempList = new ArrayList<>();
        for(Flight a:flightList){
            if(a.getAirliner().equals(selected)){
             tempList.add(a);
            }
        }
        return tempList;
    }
     
    public List<Flight> getSearchList(String from, String to){
        ArrayList<Flight> tempList = new ArrayList<>();
        for(Flight vs:flightList){
            if(vs.getDeparture().equals(from) && vs.getDestination().equals(to)){
             tempList.add(vs);
            }
        }
        return tempList;
    }
    
       public void deleteFlight(Flight a){
        flightList.remove(a);
    }
     
}

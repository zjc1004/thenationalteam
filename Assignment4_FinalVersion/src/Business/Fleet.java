/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JackZhang
 */
public class Fleet {
    private static FlightInformation FlightLists;
    public List<Airplane> fleetList;
    private Fleet abc;
    
    public Fleet() {
        fleetList=new ArrayList<>();
        
        Airplane ap1 = new Airplane("UA", "1", 1, 1, 200,200);
        Airplane ap2 = new Airplane("UA", "1", 1, 1, 200,200);
        Airplane ap3 = new Airplane("DAL", "2", 1, 1, 200,200);
        Airplane ap4 = new Airplane("DAL", "2", 1, 1, 200,200);
        Airplane ap5 = new Airplane("AA", "3", 1, 1, 200,200);
        Airplane ap6 = new Airplane("AA", "3", 1, 1, 200,200);
        
        fleetList.add(ap1);
        fleetList.add(ap2);
        fleetList.add(ap3);
        fleetList.add(ap4);
        fleetList.add(ap5);
        fleetList.add(ap6);
        
    }
    
    public Airplane addAirplane(){
        Airplane newAirplane = new Airplane();
        fleetList.add(newAirplane);
         return newAirplane;
    }
    
    public Fleet getfleetList() throws ParseException{
      abc = new Fleet();
      return abc;
    }
    
    public List<Airplane> getList(){
        return fleetList;
    }
    
    public List<Airplane> FindSelected(String selected){
        ArrayList<Airplane> tempList = new ArrayList<>();
        for(Airplane a:fleetList){
            if(a.getAirliner().equals(selected)){
             tempList.add(a);
            }
        }
        return tempList;
    }
    
    public void deleteAirplane(Airplane a){
        fleetList.remove(a);
    }
    
    
}

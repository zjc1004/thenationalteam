/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.util;

import business.item.ForsterOrder;
import business.item.HospitalOrder;
import business.item.Item;
import business.item.Pet;
import business.item.ShopOrder;
import business.organization.Hospital;
import business.organization.Organization;
import business.organization.PetForster;
import business.organization.PetFoodStore;
import business.role.Admin;
import business.role.Role;
import business.role.User;
//import com.sun.org.apache.xml.internal.security.signature.ObjectContainer;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author CBB
 */
public class SystemUtil {

    private static final String dbPath = Paths.get("systemUtil.db4o").toAbsolutePath().toString();
    public static final Set<String> mon31d = new HashSet<>(Arrays.asList("1","3","5","7","8","10","12"));
    public static final Set<String> mon30d = new HashSet<>(Arrays.asList("4","6","9","11"));
    //private static ObjectContainer db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), dbPath);
    
    /**
     * 
     * @param username
     * @param pw
     * @return null if no record is found; 
     */
    
    synchronized public static void loadSystem() {
        File file = new File(dbPath);
        if (file.exists())
            return;
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            Admin admin = new Admin("admin", "adminpw");
            Hospital h1 = new Hospital("Hospital 1", "New York"); 
            PetFoodStore p1 = new PetFoodStore("PetFoodStore 1", "New York");
            PetForster f1 = new PetForster("Forster 1", "New York");
            db.store(admin);
            db.store(h1);
            db.store(p1);
            f1.loadForster(3, 4, 6);
            db.store(f1);
            Hospital h2 = new Hospital("Hospital 2", "Boston"); 
            PetFoodStore p2 = new PetFoodStore("PetFoodStore 2", "Boston");
            PetForster f2 = new PetForster("Forster 2", "Boston");
            db.store(h2);
            db.store(p2);
            f2.loadForster(5, 3, 7);
            db.store(f2);
            Hospital h3 = new Hospital("Hospital 3", "Chicago"); 
            PetFoodStore p3 = new PetFoodStore("PetFoodStore 3", "Chicago");
            PetForster f3 = new PetForster("Forster 3", "Chicago");
            db.store(h3);
            db.store(p3);
            f3.loadForster(8, 1, 4);
            db.store(f3);
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
    
    synchronized public static Role login(String username, char[] pw) {
        String password = new String(pw);
        ObjectContainer db = null;
        Role found = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            System.out.println(db);
            //ObjectSet result = db.query(User.class);
            ObjectSet result = db.queryByExample(new Role(username, password));
            //System.out.println(result.size());
            if (result.hasNext()) {
                found = (Role)result.next();
                System.out.println("Logged in successfully.");
            } else {
                System.out.println("No record found.");
                //TODO: display something indicating account info is invalid
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
        
        return found;
    }

    synchronized public static Object userRegister(String username, char[] pw1, char[] pw2) {
        ObjectContainer db = null;
        String password = new String(pw1);
        String confirmPassword = new String(pw2);
        User newUser = null;
        try {
            if (password.isEmpty() || !password.equals(confirmPassword) || confirmPassword.isEmpty()) {
                System.out.println("Invalid password.");
            } else {
                db = Db4oEmbedded.openFile(dbPath);
                ObjectSet check = db.queryByExample(new User(username, null));
                if (check.size() == 0) {
                    newUser = new User(username, password);
                    db.store(newUser);
                    System.out.println("User registered.");
                }
                else{
                    System.out.println("User already exists.");
                }
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return newUser;
    }

    synchronized public static List<Hospital> getHospitalList() {
        List<Hospital> list = new ArrayList<>();
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(new Hospital(null, null));
            while (result.hasNext()) {
                list.add((Hospital)result.next());
            } 
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return list;
    }
    
    synchronized public static List<Organization> getOrganizationsByLocation(String location) {
        List<Organization> list = new ArrayList<>();
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(new Organization(null, location));
            System.out.println(result.size() + " organizations found in " + location);
            while (result.hasNext()) {
                list.add((Organization)result.next());
            } 
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return list;
    }
    
    synchronized public static HospitalOrder addHospitalOrder(HospitalOrder order) {
        //List<Hospital> list = new ArrayList<>();
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            db.store(order);
            System.out.println("Stored hospital order. " + order.getUser()+ " " + order.getHospitalName());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return order;
    }
    
    synchronized public static ShopOrder addPetStoreOrder(ShopOrder order) {
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            db.store(order);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return order;
    }
    
    synchronized public static ForsterOrder addForsterOrder(ForsterOrder order) {
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            db.store(order);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return order;
    }
    
    synchronized public static void updateOrganization(Organization og){
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(og);
            Organization found = null;
            if (result.hasNext()) {
                found = (Organization) result.next();
                db.delete(found);
                db.store(og);
            }
            else{
                db.store(og);
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
    
    synchronized public static void updateItem(Item item){
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(item);
            Item found = null;
            if (result.hasNext()) {
                found = (Item) result.next();
                db.delete(found);
                db.store(item);
            }
            else{
                db.store(item);
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
    
    synchronized public static ConcurrentHashMap<Pet, Integer> getCartInfo(User user, PetFoodStore store) {
        ObjectContainer db = null;
        ConcurrentHashMap<Pet, Integer> cart = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(store);
            PetFoodStore found = null;
            if (result.hasNext()) {
                found = (PetFoodStore)result.next();
            }
            else{
                //TODO: display some error info
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return cart;
    }
    
    synchronized public static ConcurrentHashMap<String, Integer> searchRemainingInfo(PetForster forster) {
        ObjectContainer db = null;
        ConcurrentHashMap<String, Integer> remaining = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(forster);
            PetForster found = null;
            if (result.hasNext()) {
                found = (PetForster)result.next();
                remaining = found.getAvailable();
            }
            else{
                //TODO: display some error info
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return remaining;
    }
    
    synchronized public static List<HospitalOrder> getAllhospitalOrders(){
        List<HospitalOrder> list = new ArrayList<>();
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(new HospitalOrder());
            //System.out.println(result.size() +  " hospitals found");
            while (result.hasNext()) {
                list.add((HospitalOrder)result.next());
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
        System.out.println(list.size() +  " hospital order records found");
        return list;
    }
    
    synchronized public static List<ShopOrder> getAllShopOrders(){
        List<ShopOrder> list = new ArrayList<>();
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(new ShopOrder());
            while (result.hasNext()) {
                list.add((ShopOrder)result.next());
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
        System.out.println(list.size() +  " shop order records found");
        return list;
    }
    
    synchronized public static List<ForsterOrder> getAllForsterOrders(){
        List<ForsterOrder> list = new ArrayList<>();
        ObjectContainer db = null;
        try {
            db = Db4oEmbedded.openFile(dbPath);
            ObjectSet result = db.queryByExample(new ForsterOrder());
            while (result.hasNext()) {
                list.add((ForsterOrder) result.next());
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
        System.out.println(list.size() +  " forster order records found");
        return list;
    }
    
    synchronized public static ConcurrentHashMap<String, Double> loadPriceTable() {
        ConcurrentHashMap<String, Double> table = new ConcurrentHashMap<>();
        table.put("Cat Food", 10.89);
        table.put("Dog Food", 13.59);
        table.put("Rabbit Food", 7.99);
        return table;
    }

}

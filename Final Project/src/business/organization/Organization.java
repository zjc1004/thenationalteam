/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import java.util.UUID;

/**
 *
 * @author sijiaxu
 */
public class Organization {
    
    private String name;
    private String location;
    private String descryption;

    public Organization(String name, String location){
        //this.id = UUID.randomUUID();
        this.name = name;
        this.location = location;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the descryption
     */
    public String getDescryption() {
        return descryption;
    }

    /**
     * @param descryption the descryption to set
     */
    public void setDescryption(String descryption) {
        this.descryption = descryption;
    }

    @Override
    public String toString() {
        return this.getName();
    }
    
    
    
}

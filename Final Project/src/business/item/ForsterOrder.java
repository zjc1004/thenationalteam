/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.item;

/**
 *
 * @author CBB
 */
public class ForsterOrder extends Item{
    private String user, forsterName, location, pet;
    private int amount, days;
    
    public ForsterOrder(){
    }

    /**
     * @return the pet
     */
    public String getPet() {
        return pet;
    }

    /**
     * @param pet the pet to set
     */
    public void setPet(String pet) {
        this.pet = pet;
    }

    /**
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return the days
     */
    public int getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(int days) {
        this.days = days;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the forsterName
     */
    public String getForsterName() {
        return forsterName;
    }

    /**
     * @param forsterName the forsterName to set
     */
    public void setForsterName(String forsterName) {
        this.forsterName = forsterName;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO
    public void userWithMostLikes(){
        Map<Integer, Integer> userLikesCount=new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        
        for (User user : users.values()) {
            for(Comment c: user.getComments()){
                int likes=0;
                if(userLikesCount.containsKey(user.getId())) {
                    likes = userLikesCount.get(user.getId());
                }
                likes += c.getLikes();
                userLikesCount.put(user.getId(), likes);
            }
        }
        int max=0; int maxId=0;
        for(int id : userLikesCount.keySet()){
            if (userLikesCount.get(id)>max) {
                max = userLikesCount.get(id);
                maxId = id;
            }
        }
        System.out.println("\n");
        System.out.println("Example: User with most likes: " + max + "\n" + users.get(maxId));
        System.out.println("\n");
    }
    
    
    
    // find 5 comments which have the most likes
    // TODO
    public void getFiveMostLikedComment(){
       Map<Integer, Comment> comments = DataStore.getInstance().getComments();
       List<Comment> commentList = new ArrayList<>(comments.values());
       
       Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
            }
       });
       
        System.out.println("Example: 5 most likes comments: ");
        for(int i=0; i<commentList.size() && i<5; i++) {
            System.out.println(commentList.get(i));
        }
        System.out.println("\n");
    }
    
    
    
    public void getAverageLikesPerCommentsComments(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        int totalLikes = 0;
        int count = 0;
        for(Comment c: commentList){
            totalLikes += c.getLikes();
            count++;
        }
        
        System.out.println("The average number of likes per comment is:" + (totalLikes/count) +"\n");
    }
    
    public void getPostWithMostLikedComment(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<Post>(posts.values());
        Map<Post, Integer>userPostCount = new HashMap<>();
        
        for(Post p: postList){
            for(Comment c: p.getComments()){
                int likes = 0;
                
                likes += c.getLikes();
                userPostCount.put(p, likes);
            }
        }
        
        int max = 0;
        int maxId = 0;
        for(Post p : userPostCount.keySet()){
            if(userPostCount.get(p)> max){
                max = userPostCount.get(p);
                maxId = p.getPostId();
            }
        }
        
        System.out.println("The Post ID with most liked comments is :"+ maxId +"\n"+"The total likes is :" + max +"\n");
        
    }
    
    public void getPostWithMostComments(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<Post>(posts.values());
        
        Comparator<Post> test = new Comparator<Post>(){
            public int compare(Post p1, Post p2){
              return  p2.getComments().size() - p1.getComments().size();
           }
        };
       
        Collections.sort(postList, test);
        
        System.out.println("The ID of post with most comment is:"+ postList.get(0).getPostId()+ ". The ID of user who posted the post is: "+ postList.get(0).getUserId());
        System.out.println();
    }        
    
    
    
    
    
// Question 4
    public void Question4() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> totalposts = new HashMap<>();

        Iterator<Map.Entry<Integer, User>> it = users.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, User> next = it.next();
            int userId = next.getKey();
            User user = next.getValue();
            totalposts.put(userId, getTotalPosts(user));
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(totalposts.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o1.getValue() - o2.getValue();
        }
    });
        
        System.out.println("Question 4: Top 5 inactive users based on total posts number: ");
        for (int i = 0; i < list.size(); i++) {
            if (i < 5) {
                User user = getUserfromMap(list.get(i).getKey());
                System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Total Posts = " + list.get(i).getValue()+"}");
            } else {
                if (list.get(i).getValue().equals(list.get(4).getValue())) { 
                    User user = getUserfromMap(list.get(i).getKey());
                    System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Total Posts = " + list.get(i).getValue()+"}");
                } else {
                    break;
                }
            }
        }
        System.out.println("\n");
    }
    

// Question 5
    public void Question5() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> totalComments = new HashMap<>();

        Iterator<Map.Entry<Integer, User>> it = users.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, User> next = it.next();
            int userId = next.getKey();
            User user = next.getValue();
            totalComments.put(userId, user.getComments().size());
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(totalComments.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o1.getValue() - o2.getValue();
        }
    });

        System.out.println("Question 5: Top 5 inactive users based on total comments they created: ");
        for (int i = 0; i < list.size(); i++) {
            if (i < 5) {
                User user = getUserfromMap(list.get(i).getKey());
                System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Total Comments = " + list.get(i).getValue()+"}");
            } else {
                if (list.get(i).getValue().equals(list.get(4).getValue())) {
                    User user = getUserfromMap(list.get(i).getKey());
                    System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Total Comments = " + list.get(i).getValue()+"}");
                } else {
                    break;
                }
            }
        }
        System.out.println("\n");
    }
    

// Question 6:
    public void Question6() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> overall = new HashMap<>();

        Iterator<Map.Entry<Integer, User>> it = users.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, User> next = it.next();
            User user = next.getValue();
            int userId = next.getKey();
            int sum = user.getComments().size() + getTotalPosts(user) + getTotalCommentsLikes(user);
            overall.put(userId, sum);
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(overall.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o1.getValue() - o2.getValue();
        }
    });

        System.out.println("Question 6: Top 5 inactive users overall: ");
        for (int i = 0; i < list.size(); i++) {
            if (i < 5) {
                User user = getUserfromMap(list.get(i).getKey());
                System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Overall = " + list.get(i).getValue()+"}");
            } else {
                if (list.get(i).getValue().equals(list.get(4).getValue())) {
                    User user = getUserfromMap(list.get(i).getKey());
                    System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Overall = " + list.get(i).getValue()+"}");
                } else {
                    break;
                }
            }
        }
        System.out.println("\n");
    }
    
// Question 7:
    public void Question7() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> overall = new HashMap<>();

        Iterator<Map.Entry<Integer, User>> it = users.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, User> next = it.next();
            User user = next.getValue();
            int userId = next.getKey();
            int sum = user.getComments().size() + getTotalPosts(user) + getTotalCommentsLikes(user);
            overall.put(userId, sum);
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(overall.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o2.getValue() - o1.getValue();
        }
    });

        System.out.println("Question 7: Top 5 proactive users overall: ");
        for (int i = 0; i < list.size(); i++) {
            if (i < 5) {
                User user = getUserfromMap(list.get(i).getKey());
                System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Overall = " + list.get(i).getValue()+"}");
            } else {
                if (list.get(i).getValue().equals(list.get(4).getValue())) {
                    User user = getUserfromMap(list.get(i).getKey());
                    System.out.println("User{id = " + list.get(i).getKey() + ", firstName = "
                        + user.getFirstName() + ", lastName = " + user.getLastName() + ", Overall = " + list.get(i).getValue()+"}");
                } else {
                    break;
                }
            }
        }
        System.out.println("\n");
    }    
    
 
    

// API    
    private User getUserfromMap(int id) {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        if (users.containsKey(id)) {
            return users.get(id);
        } else {
            return null;
        }
    }
    
    public int getTotalPosts(User u) {
        Map<Integer, Post> Posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<>(Posts.values());
        int totalPosts = 0;
        for (Post p : postList) {
            if (p.getUserId() == u.getId()) {
                totalPosts++;
            }
        }
        return totalPosts;
    }
    
    public int getTotalCommentsLikes(User u) {
        int totalCommentsLikes = 0;
        for (Comment c : u.getComments()) {
            totalCommentsLikes = totalCommentsLikes + c.getLikes();
        }
        return totalCommentsLikes;
    }
}
